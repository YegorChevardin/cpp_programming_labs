#include "entity.h"
#include <gtest/gtest.h>

TEST(test_example, NegativeNos) {
    int expected = 3;
    const string INPUT = "assets/input.txt";
    DynamicArrayContainer worksArrayContainer("0");

    worksArrayContainer.getFromFile(INPUT);
    auto *Zhenya = new ModalWork();
    Zhenya->setStudentLastName("Zhenya");
    worksArrayContainer.addElement(*Zhenya);
    worksArrayContainer.sortWorksByQuestions();
    free(Zhenya);

    ASSERT_EQ(expected, worksArrayContainer.getSize());
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
