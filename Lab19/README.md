# Laboratory work №19. OOP. Operators reload

## Target

Gain skills in developing C ++ programs that have reload of operators.

## 1 Requirements

### 1.1 Developer

Information

- Chevardin Yehor Dmutrovuch;
- group КН-921б (CIT-121b);

### 1.2 Actual task

- Rebuild laboratory work number 17 (add reloads to some operators).
- Not tu use `using namespace std`, instead of it use sentences like `using std::string`

### 1.3 Task solving
- Add reloads operators to ModalWork and DynamicArray classes


> To start compile the program use `make` bush command. Than to start it use command below in the terminal:
>
> `./dist/main.bin`.
>
> To make a documentation use: `make docgen`.
>
> To make a module tests use: `make test`.

The result of execured program:

- Result of executed file in `dist/output.txt`

```bash
[
0 => Work checked: 0, student name: Sheremet, mark: 90, amount of theory:5, amount of practice:5, amount of open tasks:5
1 => Work checked: 0, student name: Kryzha, mark: 100, amount of theory:5, amount of practice:5, amount of open tasks:5
2 => Work checked: 0, student name: Yegor, mark: 100, amount of theory:5, amount of practice:10, amount of open tasks:5
]
After filtration: 
[
0 => Work checked: 0, student name: Zhenya, mark: 0, amount of theory:5, amount of practice:5, amount of open tasks:5
1 => Work checked: 0, student name: Sheremet, mark: 90, amount of theory:5, amount of practice:5, amount of open tasks:5
2 => Work checked: 0, student name: Kryzha, mark: 100, amount of theory:5, amount of practice:5, amount of open tasks:5
]
```

## Conclusion

In this laboratory work i learned how to work and do reloads to operators in C++ language.