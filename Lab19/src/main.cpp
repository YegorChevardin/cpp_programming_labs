/**
 * # Laboratory work number 19
 * Work with classes
 * @author Chevardin Y.
 * @date 15-May-2022
 * @version 1.0.0
 */

#include "entity.h"

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	const string INPUT = "assets/input.txt";
	const string OUTPUT = "assets/output.txt";

	DynamicArrayContainer worksArrayContainer("0");

	worksArrayContainer.getFromFile(INPUT);

	cout << worksArrayContainer.toString();

	cout << "After filtration: " << endl;

	ModalWork Zhenya;
	Zhenya.setStudentLastName("Zhenya");
	worksArrayContainer.addElement(Zhenya);
	worksArrayContainer.sortWorksByQuestions();
	worksArrayContainer.writeToFile(OUTPUT);

	cout << worksArrayContainer.toString();

	return 0;
}
