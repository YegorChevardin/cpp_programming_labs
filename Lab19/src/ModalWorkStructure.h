#pragma once
#ifndef MODALWORKSTRUCTUREH
#define MODALWORKSTRUCTUREH

#include <iostream>
#include <string>
#include <sstream>

using std::endl;
using std::string;
using std::stringstream;

/** Class of modal work structure */
class ModalWorkStructure {
    private:
	int amount_of_practical_exercises;
	int amount_of_theoretical_questions;
	int amount_of_open_exercises;

    public:
	/** ModalWorkStructure default constructor */
	ModalWorkStructure()
		: amount_of_practical_exercises(5),
		  amount_of_theoretical_questions(5),
		  amount_of_open_exercises(5)
	{
	}

	/** ModalWorkStructure regular constructor */
	explicit ModalWorkStructure(string input);

	/** ModalWorkStructure copy constructor */
	ModalWorkStructure(const ModalWorkStructure &copy);

	/** ModalWorkStructure destructor */
	~ModalWorkStructure();

	int getAmountOfPracticalExercises() const;
	void setAmountOfPracticalExercises(int practise);

	int getAmountOfTheoreticalQuestions() const;
	void setAmountOfTheoreticalQuestions(int theory);

	int getAmountOfOpenExercises() const;
	void setAmountOfOpenExercises(int open_task);

	/** Returns sum of all tasks
     * @return sum all tasks in summary */
	int getAmountOfTasksInSummary() const;

	/** Converts ModalWorkStructure to string
     * @return string with objects data
     */
	string ModalWorkStructureToString() const;
};

#endif