#pragma once
#ifndef MODALWORKH
#define MODALWORKH

#include "ModalWorkStructure.h"

using std::istream;
using std::ostream;

/** Max mark that students can get */
#define MAX_MARK 100

/** Subjects for students */
enum WorkSubject { PROGRAMMING, ALGORITHMS, ELECTRONIC };

using std::cout;

/** Class of modal control work */
class ModalWork {
    private:
	bool checked;
	string student_last_name;
	int mark;
	ModalWorkStructure work_structure;
	WorkSubject work_subject;

    public:
	/** ModalWork default constructor */
	ModalWork()
		: checked(false), student_last_name("Noname"), mark(0),
		  work_structure(), work_subject(PROGRAMMING)
	{
	}

	/** ModalWork regular constructor */
	explicit ModalWork(string input);

	/** ModalWork copy constructor */
	ModalWork(const ModalWork &copy);

	/** ModalWork destructor */
	~ModalWork();

	bool getChecked() const;
	void setChecked(bool checked);

	string getStudentLastName() const;
	void setStudentLastName(string student_last_name);

	int getMark() const;
	void setMark(int mark);

	ModalWorkStructure &getWorkStructure();
	void setWorkStructure(const ModalWorkStructure &work_structure);

	WorkSubject getWorkSubject() const;
	void setWorkSubject(WorkSubject work_subject);

	/** Converts object to string
     * @return string with objects data
     */
	string ModalWorkToString() const;

	ModalWork &operator=(const ModalWork &obj);

	bool operator==(const ModalWork &obj) const;

	bool operator!=(const ModalWork &obj) const;

	friend ostream &operator<<(ostream &output, const ModalWork &object);

	friend istream &operator>>(istream &input_stream, ModalWork &object);
};
#endif