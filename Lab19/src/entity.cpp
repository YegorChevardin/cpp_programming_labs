#include "entity.h"

DynamicArrayContainer::DynamicArrayContainer(const string &input)
{
	stringstream input_stream(input);

	int size;

	input_stream >> size;

	DynamicArrayContainer::array = new ModalWork[size];
	DynamicArrayContainer::size = size;
}
DynamicArrayContainer::DynamicArrayContainer(const DynamicArrayContainer &copy)
{
	DynamicArrayContainer::size = copy.size;
	DynamicArrayContainer::array = copy.array;
}

DynamicArrayContainer::~DynamicArrayContainer()
{
	delete[] DynamicArrayContainer::array;
};

ModalWork &DynamicArrayContainer::getArray()
{
	return *DynamicArrayContainer::array;
}

int DynamicArrayContainer::getSize() const
{
	return DynamicArrayContainer::size;
}

void DynamicArrayContainer::addElement(const ModalWork &element,
				       size_t position)
{
	auto *new_array = new ModalWork[(DynamicArrayContainer::size + 1)];
start_checking_position:
	if (position > DynamicArrayContainer::size) {
		position = DynamicArrayContainer::size + 1;
		for (int i = 0; i < DynamicArrayContainer::size; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}
		new_array[position - 1] = element;
	} else if (position == 0) {
		position = 1;
		goto start_checking_position;
	} else {
		for (int i = 0; i < position - 1; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}

		new_array[position - 1] = element;

		for (int i = position; i < DynamicArrayContainer::size + 1;
		     i++) {
			new_array[i] = DynamicArrayContainer::array[i - 1];
		}
	}

	delete[] DynamicArrayContainer::array;
	DynamicArrayContainer::array = new_array;
	DynamicArrayContainer::size++;
}

void DynamicArrayContainer::removeElement(size_t index)
{
	auto *new_array = new ModalWork[DynamicArrayContainer::size - 1];

	if (index > DynamicArrayContainer::size - 1) {
		index = DynamicArrayContainer::size - 1;

		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}
	} else {
		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}

		for (int i = DynamicArrayContainer::size - 1; i > index; i--) {
			new_array[i - 1] = DynamicArrayContainer::array[i];
		}
	}
	delete[] DynamicArrayContainer::array;
	DynamicArrayContainer::array = new_array;
	DynamicArrayContainer::size--;
}

ModalWork &DynamicArrayContainer::getElement(size_t index)
{
	if (index > DynamicArrayContainer::size - 1) {
		index = DynamicArrayContainer::size - 1;
	}

	return DynamicArrayContainer::array[index];
}

string DynamicArrayContainer::toString() const
{
	stringstream output;

	output << "[" << endl;
	for (int i = 0; i < DynamicArrayContainer::size; i++) {
		output << i << " => "
		       << DynamicArrayContainer::array[i].ModalWorkToString();
	}
	output << "]" << endl;

	string result = output.str();

	return result;
}

void DynamicArrayContainer::sortWorksByQuestions()
{
	for (int i = 0; i < DynamicArrayContainer::size; i++) {
		if (DynamicArrayContainer::getElement(i)
				    .getWorkStructure()
				    .getAmountOfOpenExercises() ==
			    DynamicArrayContainer::getElement(i)
				    .getWorkStructure()
				    .getAmountOfTheoreticalQuestions() &&
		    DynamicArrayContainer::getElement(i)
				    .getWorkStructure()
				    .getAmountOfOpenExercises() ==
			    DynamicArrayContainer::getElement(i)
				    .getWorkStructure()
				    .getAmountOfPracticalExercises()) {
			continue;
		} else {
			DynamicArrayContainer::removeElement(i);
			i--;
		}
	}
}

void DynamicArrayContainer::getFromFile(const string &file_src)
{
	ifstream indata;
	string data;

	indata.open(file_src);
	if (!indata) {
		cerr << "Error file could not be opened" << endl;
		exit(1);
	}

	while (!indata.eof()) {
		getline(indata, data);
		ModalWork item(data);
		DynamicArrayContainer::addElement(item);
	}
	indata.close();
}

void DynamicArrayContainer::writeToFile(const string &file_src)
{
	ofstream ofdata;

	ofdata.open(file_src);
	for (int i = 0; i < DynamicArrayContainer::size; i++) {
		ofdata << DynamicArrayContainer::array[i].ModalWorkToString()
		       << endl;
	}
	ofdata.close();
}

ModalWork &DynamicArrayContainer::operator[](size_t i) const
{
	if (i < DynamicArrayContainer::size) {
		return array[i];
	} else {
		cout << "Index out of range, last array element returned!"
		     << endl;
		return DynamicArrayContainer::array[DynamicArrayContainer::size -
						    1];
	}
}

ostream &operator<<(ostream &output, const DynamicArrayContainer &object)
{
	output << object.toString();
	return output;
}

istream &operator>>(istream &input_stream, DynamicArrayContainer &object)
{
	string input;
	input_stream >> input;

	ModalWork item(input);
	object.addElement(item);

	return input_stream;
}
