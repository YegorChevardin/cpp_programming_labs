#include "ModalWork.h"

ModalWork::ModalWork(string input)
{
	stringstream input_stream(input);

	string subject;
	int theoretic, practise, open;

	input_stream >> ModalWork::checked;
	input_stream >> ModalWork::student_last_name;
	input_stream >> ModalWork::mark;
	input_stream >> subject;

	input_stream >> open;
	input_stream >> theoretic;
	input_stream >> practise;

	if (!(ModalWork::checked == 1 || ModalWork::checked == 0)) {
		ModalWork::checked = true;
	}

	if (ModalWork::mark > MAX_MARK) {
		ModalWork::mark = MAX_MARK;
	}

	if (subject == "PROGRAMMING") {
		ModalWork::work_subject = PROGRAMMING;
	} else if (subject == "ALGORITHMS") {
		ModalWork::work_subject = ALGORITHMS;
	} else if (subject == "ELECTRONIC") {
		ModalWork::work_subject = ELECTRONIC;
	} else {
		ModalWork::work_subject = PROGRAMMING;
	}

	ModalWork::work_structure.setAmountOfOpenExercises(open);
	ModalWork::work_structure.setAmountOfTheoreticalQuestions(theoretic);
	ModalWork::work_structure.setAmountOfPracticalExercises(practise);
}

ModalWork::ModalWork(const ModalWork &copy)
{
	ModalWork::checked = copy.checked;
	ModalWork::student_last_name = copy.student_last_name;
	ModalWork::mark = copy.mark;
	ModalWork::work_structure = copy.work_structure;
	ModalWork::work_subject = copy.work_subject;
}

ModalWork::~ModalWork() = default;

bool ModalWork::getChecked() const
{
	return ModalWork::checked;
}
void ModalWork::setChecked(bool checked)
{
	ModalWork::checked = checked;
}

string ModalWork::getStudentLastName() const
{
	return ModalWork::student_last_name;
}
void ModalWork::setStudentLastName(string student_last_name)
{
	ModalWork::student_last_name = student_last_name;
}

int ModalWork::getMark() const
{
	return ModalWork::mark;
}
void ModalWork::setMark(int mark)
{
	if (mark < MAX_MARK) {
		ModalWork::mark = mark;
	} else {
		cout << "Mark is over 100, corruption!" << endl;
	}
}

ModalWorkStructure &ModalWork::getWorkStructure()
{
	return ModalWork::work_structure;
}
void ModalWork::setWorkStructure(const ModalWorkStructure &work_structure)
{
	ModalWork::work_structure = work_structure;
}

WorkSubject ModalWork::getWorkSubject() const
{
	return ModalWork::work_subject;
}
void ModalWork::setWorkSubject(WorkSubject work_subject)
{
	ModalWork::work_subject = work_subject;
}

string ModalWork::ModalWorkToString() const
{
	stringstream output;

	output << "Work checked: " << ModalWork::checked
	       << ", student name: " << ModalWork::student_last_name
	       << ", mark: " << ModalWork::mark << ", "
	       << ModalWork::work_structure.ModalWorkStructureToString();

	string result = output.str();

	return result;
}

ModalWork &ModalWork::operator=(const ModalWork &obj)
{
	{
		if (this == &obj) {
			return *this;
		}

		ModalWork::checked = obj.checked;
		ModalWork::work_structure = obj.work_structure;
		ModalWork::work_subject = obj.work_subject;
		ModalWork::mark = obj.mark;
		ModalWork::student_last_name = obj.student_last_name;

		return *this;
	}
}

bool ModalWork::operator==(const ModalWork &obj) const
{
	return (ModalWork::checked == obj.checked &&
		ModalWork::work_structure.getAmountOfTasksInSummary() ==
			obj.work_structure.getAmountOfTasksInSummary() &&
		ModalWork::work_subject == obj.work_subject &&
		ModalWork::mark == obj.mark &&
		ModalWork::student_last_name == obj.student_last_name);
}

bool ModalWork::operator!=(const ModalWork &obj) const
{
	return this != &obj;
}

ostream &operator<<(ostream &output, const ModalWork &object)
{
	output << object.ModalWorkToString();
	return output;
}

istream &operator>>(istream &input_stream, ModalWork &object)
{
	string subject;
	int theoretic, practise, open;

	input_stream >> object.checked;
	input_stream >> object.student_last_name;
	input_stream >> object.mark;
	input_stream >> subject;

	input_stream >> open;
	input_stream >> theoretic;
	input_stream >> practise;

	if (!(object.checked == 1 || object.checked == 0)) {
		object.checked = true;
	}

	if (object.mark > MAX_MARK) {
		object.mark = MAX_MARK;
	}

	if (subject == "PROGRAMMING") {
		object.work_subject = PROGRAMMING;
	} else if (subject == "ALGORITHMS") {
		object.work_subject = ALGORITHMS;
	} else if (subject == "ELECTRONIC") {
		object.work_subject = ELECTRONIC;
	} else {
		object.work_subject = PROGRAMMING;
	}

	object.work_structure.setAmountOfOpenExercises(open);
	object.work_structure.setAmountOfTheoreticalQuestions(theoretic);
	object.work_structure.setAmountOfPracticalExercises(practise);

	return input_stream;
}
