#pragma once
#ifndef ENTITYH
#define ENTITYH

#include "ModalWork.h"
#include "ModalWorkStructure.h"

#include <fstream>

using std::cerr;
using std::ifstream;
using std::ofstream;

/** Default dynamic array size (can be changed) */
#define DEFAULT_DYNAMIC_ARRAY_SIZE 1

/** Class that stands for dynamic array container */
class DynamicArrayContainer {
    private:
	/** Array of ModalWork objects */
	ModalWork *array{};
	/** Size of array of ModalWork objects */
	int size;

    public:
	/** DynamicArrayContainer default constructor */
	DynamicArrayContainer() : size(DEFAULT_DYNAMIC_ARRAY_SIZE)
	{
	}
	/** DynamicArrayContainer regular constructor
     * @param input String with objects data (must include template: [%d %s %d %s %d %d %d])
     */
	explicit DynamicArrayContainer(const string &input);

	/** DynamicArrayContainer copy constructor */
	DynamicArrayContainer(const DynamicArrayContainer &copy);

	/** DynamicArrayContainer destructor */
	~DynamicArrayContainer();

	ModalWork &getArray();

	int getSize() const;

	/** Adds element to array
     * @param position position (index + 1) to which element will be placed
     * @param element object of ModalWorks class that will be placed to array
     */
	void addElement(const ModalWork &element, size_t position = 1);

	/** Removes element from array
     * @param index index of element that will be removed
     */
	void removeElement(size_t index);

	/** Returns an actual element from array
     * @param index index of element that will be returned
     * @return element of requested index*/
	ModalWork &getElement(size_t index);

	/** Converts array to string
     * @return string with array data*/
	string toString() const;

	/** Sorting array by questions (if object will have the same amount of exercises of all kinds - he will stay in array) */
	void sortWorksByQuestions();

	/** Gets data from file, converts it to objects and paste it into array
     * @param file_src file directory
     */
	void getFromFile(const string &file_src);

	/** Writes array data to file
     * @param file_src file output directory
     */
	void writeToFile(const string &file_src);

	ModalWork &operator[](size_t i) const;

	friend ostream &operator<<(ostream &output,
				   const DynamicArrayContainer &object);

	friend istream &operator>>(istream &input_stream,
				   DynamicArrayContainer &object);
};
#endif
