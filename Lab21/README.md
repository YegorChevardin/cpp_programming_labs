# Laboratory work №21. OOP. Template functions and classes

## Target

Gain skills in developing C ++ programs that have dynamic arrays created via classes.

## 1 Requirements

### 1.1 Developer

Information

- Chevardin Yehor Dmutrovuch;
- group КН-921б (CIT-121b);

### 1.2 Task

- Created dynamic array for any type of data, using classes;

### 1.3 Actual task

• Create a template class-list (based on a dynamic array) that has a template field
array (for any existing data type)
• Create the following methods:

- output the contents of the array to the screen;
- determine the index of the transmitted element in a given array;
- sort array elements;
- determine the value of the minimum element of the array;
- add an element to the end of the array;
- delete an element from the array by index.

### 1.4 Result

> To start compile the program use `make` bush command. Than to start it use command below in the terminal:
>
> `./dist/main.bin`.
>
> To make a documentation use: `make docgen`.
>
> To make a module tests use: `make test`.

The result of executed program:

```bash
[0 => 655; 1 => 960; 2 => 450; 3 => 901; 4 => 629; 5 => 708; 6 => 982; 7 => 518; 8 => 778; 9 => 673]
After sorting: 
[0 => 450; 1 => 518; 2 => 629; 3 => 655; 4 => 673; 5 => 708; 6 => 778; 7 => 901; 8 => 960; 9 => 982]
After adding element and get min value: 
[0 => 0; 1 => 450; 2 => 518; 3 => 629; 4 => 655; 5 => 673; 6 => 708; 7 => 778; 8 => 901; 9 => 960]
Min element: 0
```

## Conclusion

In this laboratory work i learned how to create dynamic array classes for any type of data in C++ language.