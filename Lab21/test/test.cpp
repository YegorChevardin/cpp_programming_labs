#include "DynamicArrayContainer.hpp"
#include <gtest/gtest.h>

TEST(test_example, NegativeNos) {
    DynamicArrayContainer<int> array_1(10);
    int expected = 0;
    int expected_size = 10;

    array_1.addElement(0);
    array_1.removeElement(array_1.getSize() - 1);
    int actual = array_1.getMinElement();

    ASSERT_EQ(expected, actual);
    ASSERT_EQ(expected_size, array_1.getSize());
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
