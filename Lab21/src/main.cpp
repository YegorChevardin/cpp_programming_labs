/**
 * # Laboratory work number 21
 * OOP. Templated functions and classes
 * @author Chevardin Y.
 * @date 21-May-2022
 * @version 1.0.0
 */

#include "DynamicArrayContainer.hpp"
#include "Test.hpp"

using std::string;

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	/* int */
	DynamicArrayContainer<int> array_1(3);
	array_1.setElement(0, 1);
	array_1.setElement(1, 2);
	array_1.setElement(2, 3);

	array_1.printArray();

	array_1.sortArray("ASC");
	cout << "After sorting: " << endl;
	array_1.printArray();

	array_1.addElement(4);
	array_1.removeElement(1);
	cout << "After adding and removing element and get min value: " << endl;
	array_1.printArray();
	cout << "Min element: " << array_1.getMinElement() << endl;

	/* string */
	DynamicArrayContainer<string> array_2(3);
	array_2.setElement(0, "Hello");
	array_2.setElement(1, " World");
	array_2.setElement(2, "!");

	array_2.printArray();

	array_2.sortArray("ASC");
	cout << "After sorting: " << endl;
	array_2.printArray();

	array_2.addElement(" No.");
	array_2.removeElement(1);
	cout << "After adding and removing element and get min value: " << endl;
	array_2.printArray();
	cout << "Min element: " << array_2.getMinElement() << endl;

	/* own class */

	DynamicArrayContainer<Test> array_3(1);
	Test test_1;
	array_3.setElement(0, test_1);

	array_3.printArray();
	return 0;
}
