#include "Test.hpp"

Test::Test() : number(11)
{
}

Test::~Test() = default;

ostream &operator<<(ostream &output, const Test &object)
{
	output << object.number;

	return output;
}
