#pragma once
#ifndef DYNAMICARRAYCONTAINER_H
#define DYNAMICARRAYCONTAINER_H

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>

using std::cout;
using std::endl;
using std::string;

/** Class that stands for dynamic array container */
template <typename T> class DynamicArrayContainer {
    private:
	/** Size of array of ModalWork objects */
	int size;
	/** Array of ModalWork objects */
	T *array;

    public:
	/** DynamicArrayContainer default constructor */
	DynamicArrayContainer();

	/** DynamicArrayContainer regular constructor
     * @param size Size of array that will be created
     */
	DynamicArrayContainer(int size);

	/** DynamicArrayContainer copy constructor */
	DynamicArrayContainer(const DynamicArrayContainer &copy);

	/** DynamicArrayContainer destructor */
	~DynamicArrayContainer();

	T &getArray() const;

	int getSize() const;

	/** Adds element to array
     * @param position position (index + 1) to which element will be placed
     * @param element object of ModalWorks class that will be placed to array
     */
	void addElement(const T element, size_t position = 1);

	/** Removes element from array
     * @param index index of element that will be removed
     */
	void removeElement(size_t index);

	/** Returns an actual element from array
     * @param index index of element that will be returned
     * @return element of requested index*/
	T getElement(size_t index) const;

	/** Set to existing element some value
     * @param index Index of the element
     * @param number Number that will be applied to the element*/
	void setElement(size_t index, T value);

	/** Printing array */
	void printArray() const;

	/** Sorting array elements by ASC or DESC */
	void sortArray(const string &way);

	/** Get index of specific element, if it exists in array
     * @param value Value of element, of what index you whand to get
     * @return Index of the element*/
	int getIndexOfElement(T value) const;

	/** Get minimum element from array
     * @return minimum element*/
	T getMinElement() const;
};

/* Init section start */

template <typename T>
DynamicArrayContainer<T>::DynamicArrayContainer() : size(1)
{
}

template <typename T> DynamicArrayContainer<T>::DynamicArrayContainer(int size)
{
	srand(time(NULL));

	this->array = new T[size];
	this->size = size;
}

template <typename T>
DynamicArrayContainer<T>::DynamicArrayContainer(
	const DynamicArrayContainer &copy)
	: size(copy.size)
{
	delete[] this->array;
	this->array = copy.array;
}

template <typename T> DynamicArrayContainer<T>::~DynamicArrayContainer()
{
	delete[] DynamicArrayContainer::array;
};

template <typename T> T &DynamicArrayContainer<T>::getArray() const
{
	return *this->array;
}

template <typename T> int DynamicArrayContainer<T>::getSize() const
{
	return this->size;
}

template <typename T>
void DynamicArrayContainer<T>::addElement(const T element, size_t position)
{
	T *new_array = new T[(this->size + 1)];

start_checking_position:
	if (position > DynamicArrayContainer::size) {
		position = DynamicArrayContainer::size + 1;
		for (int i = 0; i < DynamicArrayContainer::size; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}
		new_array[position - 1] = element;
	} else if (position == 0) {
		position = 1;
		goto start_checking_position;
	} else {
		for (int i = 0; i < position - 1; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}

		new_array[position - 1] = element;

		for (int i = position; i < DynamicArrayContainer::size + 1;
		     i++) {
			new_array[i] = DynamicArrayContainer::array[i - 1];
		}
	}

	delete[] DynamicArrayContainer::array;
	DynamicArrayContainer::array = new_array;
	DynamicArrayContainer::size++;
}

template <typename T> void DynamicArrayContainer<T>::removeElement(size_t index)
{
	T *new_array = new T[this->size - 1];

	if (index > DynamicArrayContainer::size - 1) {
		index = DynamicArrayContainer::size - 1;

		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}
	} else {
		for (int i = 0; i < index; i++) {
			new_array[i] = DynamicArrayContainer::array[i];
		}

		for (int i = DynamicArrayContainer::size - 1; i > index; i--) {
			new_array[i - 1] = DynamicArrayContainer::array[i];
		}
	}
	delete[] DynamicArrayContainer::array;
	DynamicArrayContainer::array = new_array;
	DynamicArrayContainer::size--;
}

template <typename T> T DynamicArrayContainer<T>::getElement(size_t index) const
{
	if (index > this->size - 1) {
		index = this->size - 1;
	}

	return this->array[index];
}

template <typename T>
void DynamicArrayContainer<T>::setElement(size_t index, T value)
{
	if (index > this->size - 1) {
		index = this->size - 1;
	}

	this->array[index] = value;
}

template <typename T> void DynamicArrayContainer<T>::printArray() const
{
	cout << "[";
	for (int i = 0; i < this->size; i++) {
		if (i == this->size - 1) {
			cout << i << " => " << this->array[i];
		} else {
			cout << i << " => " << this->array[i] << "; ";
		}
	}
	cout << "]" << endl;
}

template <typename T>
void DynamicArrayContainer<T>::sortArray(const string &way)
{
	if (way == "ASC") {
		for (int i = 0; i < this->size; i++) {
			for (int j = this->size - 1; j > i; j--) {
				if (this->array[j - 1] < this->array[j]) {
					T x = array[j - 1];
					this->array[j - 1] = this->array[j];
					this->array[j] = x;
				}
			}
		}
	} else if (way == "DESC") {
		for (int i = 0; i < this->size; i++) {
			for (int j = this->size - 1; j > i; j--) {
				if (this->array[j - 1] > this->array[j]) {
					T x = this->array[j - 1];
					this->array[j - 1] = this->array[j];
					this->array[j] = x;
				}
			}
		}
	} else {
		cout << "Please, type correct way of sorting!" << endl;
	}
}

template <typename T>
int DynamicArrayContainer<T>::getIndexOfElement(T value) const
{
	int index_to_find;

	for (int i = 0; i < this->size; i++) {
		if (this->array[i] == value) {
			index_to_find = i;
		}
	}

	return index_to_find;
}

template <typename T> T DynamicArrayContainer<T>::getMinElement() const
{
	T min_element = this->array[0];

	for (int i = 0; i < this->size; i++) {
		if (min_element > this->array[i]) {
			min_element = this->array[i];
		}
	}

	return min_element;
}
#endif //DYNAMICARRAYCONTAINER_H
