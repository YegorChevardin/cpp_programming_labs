#pragma once
#ifndef LAB21_TEST_H
#define LAB21_TEST_H

#include <iostream>

using std::istream;
using std::ostream;

class Test {
    private:
	int number;

    public:
	Test();
	~Test();

	int getNumber() const;
	void setNumber(int value);

	friend ostream &operator<<(ostream &output, const Test &object);
};

#endif //LAB21_TEST_H
