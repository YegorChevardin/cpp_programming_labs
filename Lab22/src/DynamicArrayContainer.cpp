#include "DynamicArrayContainer.h"

DynamicArrayContainer::DynamicArrayContainer()
{
}

DynamicArrayContainer::DynamicArrayContainer(int size)
{
	srand(time(NULL));

	for (int i = 0; i < size; i++) {
		this->array.emplace_back(rand() % 10);
	}
}

DynamicArrayContainer::DynamicArrayContainer(const DynamicArrayContainer &copy)
{
	for (int i = 0; i < this->array.size(); i++) {
		this->array.push_back(copy.array[i]);
	}
}

DynamicArrayContainer::~DynamicArrayContainer() = default;

int DynamicArrayContainer::getSize() const
{
	return this->array.size();
}

void DynamicArrayContainer::addElement(const int element, size_t position)
{
	this->array.insert(this->array.begin() + position - 1, element);
}

void DynamicArrayContainer::removeElement(const size_t index)
{
	this->array.erase(this->array.begin() + index);
}

int DynamicArrayContainer::getElement(size_t index) const
{
	if (index > this->array.size()) {
		index = this->array.size() - 1;
	}

	return this->array[index];
}

void DynamicArrayContainer::setElement(size_t index, int number)
{
	if (index > this->array.size()) {
		index = this->array.size() - 1;
	}

	this->array[index] = number;
}

void DynamicArrayContainer::printArray() const
{
	cout << "[";
	for (int value : this->array) {
		cout << value << " ";
	}
	cout << "]" << endl;
}

int DynamicArrayContainer::getIndexOfElement(int value) const
{
	auto it = find(this->array.begin(), this->array.end(), value);

	if (it != this->array.end()) {
		int index = it - this->array.begin();
		return index;
	} else {
		cout << "No element found!" << endl;
		return -1;
	}
}

void DynamicArrayContainer::sortArray(const string way)
{
	if (way == "ASC") {
		this->sort_by_asc(&this->array);
	} else if (way == "DESC") {
		this->sort_by_desc(&this->array);
	} else {
		cout << "You typed a wrong way to sort!" << endl;
	}
}
