#include "DynamicArraySortByAsc.h"

void DynamicArraySortByAsc::operator()(vector<int> *array)
{
	sort(array->begin(), array->end());
}