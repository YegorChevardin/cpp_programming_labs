#pragma once
#ifndef LAB22_DYNAMICARRAYSORTBYASC_H
#define LAB22_DYNAMICARRAYSORTBYASC_H

#include <vector>
#include <bits/stdc++.h>

using std::sort;
using std::vector;

class DynamicArraySortByAsc {
    public:
	void operator()(vector<int> *array);
};
#endif //LAB22_DYNAMICARRAYSORTBYASC_H
