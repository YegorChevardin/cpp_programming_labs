#ifndef LAB22_DYNAMICARRAYSORTBYDESC_H
#define LAB22_DYNAMICARRAYSORTBYDESC_H

#include <vector>
#include <bits/stdc++.h>

using std::greater;
using std::sort;
using std::vector;

class DynamicArraySortByDesc {
    public:
	void operator()(vector<int> *array);
};

#endif //LAB22_DYNAMICARRAYSORTBYDESC_H
