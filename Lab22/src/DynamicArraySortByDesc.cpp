#include "DynamicArraySortByDesc.h"

void DynamicArraySortByDesc::operator()(vector<int> *array)
{
	sort(array->begin(), array->end(), greater<int>());
}
