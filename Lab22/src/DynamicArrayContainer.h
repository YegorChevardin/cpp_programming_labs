#pragma once
#ifndef DYNAMICARRAYCONTAINER_H
#define DYNAMICARRAYCONTAINER_H

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include "DynamicArraySortByAsc.h"
#include "DynamicArraySortByDesc.h"

using std::cout;
using std::endl;
using std::find;
using std::string;

/** Class that stands for dynamic array container */
class DynamicArrayContainer {
    private:
	vector<int> array;
	DynamicArraySortByDesc sort_by_desc;
	DynamicArraySortByAsc sort_by_asc;

    public:
	/** DynamicArrayContainer default constructor */
	DynamicArrayContainer();

	/** DynamicArrayContainer regular constructor
     * @param size Size of array that will be created
     */
	DynamicArrayContainer(int size);

	/** DynamicArrayContainer copy constructor */
	DynamicArrayContainer(const DynamicArrayContainer &copy);

	/** DynamicArrayContainer destructor */
	~DynamicArrayContainer();

	/** Get size of array
     * @return Size of array*/
	int getSize() const;

	/** Adds element to array
     * @param position position (index + 1) to which element will be placed
     * @param element object of ModalWorks class that will be placed to array
     */
	void addElement(const int element, size_t position = 1);

	/** Removes element from array
     * @param index index of element that will be removed
     */
	void removeElement(size_t index = 0);

	/** Returns an actual element from array
     * @param index index of element that will be returned
     * @return element of requested index*/
	int getElement(size_t index) const;

	/** Set to existing element some value
     * @param index Index of the element
     * @param number Number that will be applied to the element*/
	void setElement(size_t index, int number);

	/** Printing array */
	void printArray() const;

	/** Sorting array elements by ASC or DESC
	 * @param way Sorting way (ASC or DESC)*/
	void sortArray(const string way);

	/** Get index of specific element, if it exists in array
     * @param value Value of element, of what index you whand to get
     * @return Index of the element*/
	int getIndexOfElement(int value) const;
};
#endif //DYNAMICARRAYCONTAINER_H
