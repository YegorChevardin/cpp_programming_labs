/**
 * # Laboratory work number 22
 * OOP. Templated functions and classes
 * @author Chevardin Y.
 * @date 21-May-2022
 * @version 1.0.0
 */

#include "DynamicArrayContainer.h"

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	DynamicArrayContainer array_1(10);

	array_1.printArray();
	array_1.sortArray("DESC");
	cout << "After sorting: " << endl;
	array_1.printArray();
	array_1.addElement(0);
	array_1.removeElement(array_1.getSize() - 1);
	cout << "After adding element: " << endl;
	array_1.printArray();

	return 0;
}
