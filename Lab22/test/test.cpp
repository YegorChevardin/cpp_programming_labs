#include "DynamicArrayContainer.h"
#include <gtest/gtest.h>

TEST(test_example, NegativeNos) {
    int expected = 10, actual;
    DynamicArrayContainer array_1(10);

    array_1.sortArray("DESC");
    array_1.addElement(0);
    array_1.removeElement(array_1.getSize() - 1);
    actual = array_1.getSize();

    ASSERT_EQ(expected, actual);
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
