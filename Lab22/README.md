# Laboratory work №22. OOP. STL. Intro to standart template library

## Target

Gain skills in developing C ++ programs that have dynamic arrays created via STL libraries.

## 1 Requirements

### 1.1 Developer

Information

- Chevardin Yehor Dmutrovuch;
- group КН-921б (CIT-121b);

### 1.2 Task

- Rebuild dynamic array class using STL vector;

### 1.3 Actual task

- Rewrite all functions to vector functions;

### 1.4 Result

> To start compile the program use `make` bush command. Than to start it use command below in the terminal:
>
> `./dist/main.bin`.
>
> To make a documentation use: `make docgen`.
>
> To make a module tests use: `make test`.

The result of executed program:

```bash
[2 4 5 5 1 3 7 0 7 7 ]
After sorting: 
[7 7 7 5 5 4 3 2 1 0 ]
After adding element: 
[0 7 7 7 5 5 4 3 2 1 ]
```

## Conclusion

In this laboratory work i learned how to create dynamic array using STL vector in C++ language.