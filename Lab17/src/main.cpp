/**
 * # Laboratory work number 17
 * Work with classes
 * @author Chevardin Y.
 * @date 1-May-2022
 * @version 1.0.0
 */

#include "lib.h"
#include "entity.h"

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	/* construct in work (default construct is without arguments) */
	DynamicArrayContainer worksArrayContainer(3);

	/* seeding array */
	worksArrayContainer.getElement(0)
		.getWorkStructure()
		.setAmountOfOpenExercises(10);
	worksArrayContainer.getElement(0).setStudentLastName("Chevardin");
	worksArrayContainer.getElement(1).setStudentLastName("Kryrza");
	worksArrayContainer.getElement(2).setStudentLastName("Sheremet");
	worksArrayContainer.print();

	cout << "After filtration: " << endl;

	ModalWork *Zhenya = new ModalWork();
	Zhenya->setStudentLastName("Zhenya");
	worksArrayContainer.addElement(*Zhenya);
	worksArrayContainer.removeElement(0);

	/* copy construct in work */
	DynamicArrayContainer new_array(worksArrayContainer);

	new_array.sortWorksByQuestions();

	new_array.print();

	return 0;
}
