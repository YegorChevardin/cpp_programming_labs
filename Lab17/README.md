# Лабораторна робота №17. ООП. Вступ до ООП

## Мета

Отримати навички розробки програм мовою С++, які написані за принципом ООП.

## 1 Вимоги

### 1.1 Розробник

Інформація

- Чевардін Єгор Дмитрович;
- группа КН-921б (КІТ-121б);

### 1.2 Загальне завдання

- Переробити лабораторну роботу номер 15 під ООП стиль.
- Зробити так, щоб модульні тести виконувались за допомогою гугл тестів

### 1.3 Задача
- Переробити структури у класи
- Зробити конструктори
- Зробити роботи згідно вимогам
- Переробити `Makefile` під гугл тести


> Для того, щоб запустити программу та побачити її роботу, достатьно виповнити bash команду:
>
> `./dist/main.bin`.
>
> Зробити документацію до проекта можна за допомогую команди `make docgen`.
>
> Зробити модульні тести до ціеї програми можна за допомогою команди `make test`.

В результаті виконання лабораторної роботи були розроблені класси, в яких є 3 типа конструкторів:

- За замовчуванням (Виконується при створенні обьєкта, якщо данні не були передані);
- Звичайний конструктор (Виконується, якщо при створенні були передані якійсь данні);
- Копіюючий конструктор (Виконується, коли треба скопіювати обьєкт);
- Також є деструктор (Виконується при видаленні обьєкта);

Приклад ціх конструкторів зображено нижче:

```cpp
//entity.h
DynamicArrayContainer() : size(DYNAMIC_ARRAY_SIZE)
	{
		DynamicArrayContainer::array =
			new ModalWork[DYNAMIC_ARRAY_SIZE];
	}
	explicit DynamicArrayContainer(int size);
	DynamicArrayContainer(const DynamicArrayContainer &copy);

	~DynamicArrayContainer();
//entity.cpp
DynamicArrayContainer::DynamicArrayContainer(int size)
{
	DynamicArrayContainer::array = new ModalWork[size];
	DynamicArrayContainer::size = size;
}
DynamicArrayContainer::DynamicArrayContainer(const DynamicArrayContainer &copy)
{
	DynamicArrayContainer::size = copy.size;
	DynamicArrayContainer::array = copy.array;
}

DynamicArrayContainer::~DynamicArrayContainer() = default;
```

Корректний результат виконання программи:

```ABAP
./dist/main.bin
Array size: 3.
Element number 1:
Work checked: 0, student name: Chevardin, mark: 0.
Amount of theory: 5, amount of practice: 5, amount of open tasks: 10
Element number 2:
Work checked: 0, student name: Kryrza, mark: 0.
Amount of theory: 5, amount of practice: 5, amount of open tasks: 5
Element number 3:
Work checked: 0, student name: Sheremet, mark: 0.
Amount of theory: 5, amount of practice: 5, amount of open tasks: 5
After filtration: 
Array size: 2.
Element number 1:
Work checked: 0, student name: Kryrza, mark: 0.
Amount of theory: 5, amount of practice: 5, amount of open tasks: 5
Element number 2:
Work checked: 0, student name: Sheremet, mark: 0.
Amount of theory: 5, amount of practice: 5, amount of open tasks: 5
```

## Висновок

На цій лабораторній работі я навчився робити програми за принципом ООП стилю C++.