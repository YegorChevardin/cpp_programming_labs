#include "lib.h"
#include "entity.h"
#include <gtest/gtest.h>

TEST(test_example, NegativeNos) {
    int expected = 2;

    /* doing some operations (in result must be 2) */
    DynamicArrayContainer worksArrayContainer(3);
    worksArrayContainer.getElement(0)
            .getWorkStructure()
            .setAmountOfOpenExercises(10);
    ModalWork *Zhenya = new ModalWork();
    Zhenya->setStudentLastName("Zhenya");
    worksArrayContainer.addElement(*Zhenya);
    worksArrayContainer.removeElement(0);
    DynamicArrayContainer new_array(worksArrayContainer);
    new_array.sortWorksByQuestions();

    ASSERT_EQ(expected, new_array.getSize());
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
