#include "Deck.h"

Deck::~Deck() = default;

Deck::Deck() : amount_of_combinations(COMBINATIONS)
{
	combinations[0] = "6C 7C 8C 9C 10C";
	combinations[1] = "9H 9D 9S 9C 7D";
	combinations[2] = "10D 10C 10H 7C 7D";
	combinations[3] = "2D 4D 6D 9D 10D";
	combinations[4] = "2S 3C 4H 5S 6H";
	combinations[5] = "9D 9S 9C 7C AD";
	combinations[6] = "10C 10D 2D 2S 8S";
	combinations[7] = "5S 5C 9D 6S 8H";
	combinations[8] = "10C 8D 6D 4D 2S";
}

int Deck::getAmountOfCombinations()
{
	return this->amount_of_combinations;
}

string *Deck::getCombinations()
{
	return this->combinations;
}