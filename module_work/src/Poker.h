#pragma once
#ifndef MODULE_WORK_POKER_H
#define MODULE_WORK_POKER_H

#include <sstream>
#include "Deck.h"

#define AMOUNT_OF_GIVEN_CARDS 5

using std::cout;
using std::endl;
using std::getline;
using std::stringstream;

class Poker {
    private:
	string cards[AMOUNT_OF_GIVEN_CARDS];
	int amount_of_given_cards;
	string result;

    public:
	Poker(string input);

	void printCards();

	string &getResult();
	void setResult();
};
#endif //MODULE_WORK_POKER_H
