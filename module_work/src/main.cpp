/**
 * # Laboratory work number 19
 * Work with classes
 * @author Chevardin Y.
 * @date 15-May-2022
 * @version 1.0.0
 */

#include "Poker.h"

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	string input;

	std::getline(std::cin, input);

	Poker poker_1(input);
	poker_1.printCards();
	poker_1.setResult();
	string result = poker_1.getResult();
	cout << result << endl;
	return 0;
}
