#ifndef MODULE_WORK_DECK_H
#define MODULE_WORK_DECK_H

#include <iostream>
#include <string>

#define COMBINATIONS 9

using std::string;

class Deck {
    private:
	string combinations[COMBINATIONS];
	int amount_of_combinations;

    public:
	/** ModalWork regular constructor */
	Deck();

	/** ModalWork destructor */
	~Deck();

	string *getCombinations();

	int getAmountOfCombinations();
};
#endif //MODULE_WORK_DECK_H
