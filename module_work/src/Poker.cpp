#include "Poker.h"

Poker::Poker(string input) : amount_of_given_cards(AMOUNT_OF_GIVEN_CARDS)
{
	stringstream input_stream(input);

	for (int i = 0; i < amount_of_given_cards; i++) {
		input_stream >> this->cards[i];
	}
}

string &Poker::getResult()
{
	return this->result;
}

void Poker::setResult()
{
	Deck deck;
	int deck_size = deck.getAmountOfCombinations();
	string *combinations = deck.getCombinations();
	int counter = 0;

	for (int j = 0; j < deck_size; j++) {
		stringstream deck_variant(combinations[j]);
		string deck_cards[this->amount_of_given_cards];

		if (counter == 5) {
			break;
		}

		for (int i = 0; i < amount_of_given_cards; i++) {
			deck_variant >> deck_cards[i];
			bool equal = false;

			for (int k = 0; k < amount_of_given_cards; k++) {
				if (deck_cards[i] == this->cards[k]) {
					counter++;
					equal = true;
					break;
				} else {
					continue;
				}
			}

			if (equal) {
				if (counter == 5) {
					break;
				}
				continue;
			} else {
				counter = 0;
				break;
			}
		}

		if (counter == 5) {
			switch (j) {
			case 0:
				this->result = "Стрит флеш";
				break;
			case 1:
				this->result = "Каре";
				break;
			case 2:
				this->result = "Фул хаус";
				break;
			case 3:
				this->result = "Флеш";
				break;
			case 4:
				this->result = "Стрит";
				break;
			case 5:
				this->result = "Три карты";
				break;
			case 6:
				this->result = "Две пары";
				break;
			case 7:
				this->result = "Пара";
				break;
			case 8:
				this->result = "Старшая карта";
				break;
			default:
				this->result = "Не найдено комбинаций";
			}
			break;
		} else {
			counter = 0;
			this->result = "Не найдено комбинаций";
		}
	}
}

void Poker::printCards()
{
	cout << "You gave cards: ";
	for (int i = 0; i < this->amount_of_given_cards; i++) {
		cout << this->cards[i] << " ";
	}
	cout << endl;
}
