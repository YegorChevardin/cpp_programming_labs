#include "Poker.h"
#include <gtest/gtest.h>

TEST(test_example, NegativeNos) {
    string input;

    std::getline(std::cin, input);

    Poker poker_1(input);
    poker_1.setResult();
    string result = poker_1.getResult();
    ASSERT_EQ(result, "Три карты");
}

int main(int argc, char* argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
