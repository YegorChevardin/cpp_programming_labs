# Laboratory work №18. OOP. FLOWS

## Target

Gain skills in developing C ++ programs that have input / output functions and file usage..

## 1 Requirements

### 1.1 Developer

Information

- Chevardin Yehor Dmutrovuch;
- group КН-921б (CIT-121b);

### 1.2 Actual task

- Rebuild laboratory work number 17 (delete C language functions).
- Add work with files (In C++ language type)
- Remake output functions, so they actualy not printing anything. They must convert data to string using stringstream
- Not tu use `using namespace std`, instead of it use sentences like `using std::string`

### 1.3 Task solving
- Retype all functions to C++ style
- Add file works functions to DynamicArrayContainerClass


> To start compile the program use `make` bush command. Than to start it use command below in the terminal:
>
> `./dist/main.bin`.
>
> To make a documentation use: `make docgen`.
>
> To make a module tests use: `make test`.

The result of execured program:

- Result of executed file in `dist/output.txt`

```bash
clang-format src/* -i
clang -c -Wall -I h -I -g -g0 -fprofile-instr-generate -fcoverage-mapping src/entity.cpp -o entity.o
clang -c -Wall -I h -I -g -g0 -fprofile-instr-generate -fcoverage-mapping src/main.cpp -o main.o
mkdir -p dist
clang -fprofile-instr-generate -fcoverage-mapping entity.o main.o -lstdc++ -o dist/main.bin
clang -c -Wall -I h -I -g -g0 -fprofile-instr-generate -fcoverage-mapping -Isrc test/test.cpp -o test.o
clang -fprofile-instr-generate -fcoverage-mapping entity.o test.o -lstdc++ -lm -I h -lgtest -lrt -pthread -lsubunit -o dist/test.bin
./dist/main.bin
Element number 1:
Work checked: 0, student name: Sheremet, mark: 90.
Amount of theory:5, amount of practice:5, amount of open tasks:5
Element number 2:
Work checked: 0, student name: Kryzha, mark: 100.
Amount of theory:5, amount of practice:5, amount of open tasks:5
Element number 3:
Work checked: 0, student name: Yegor, mark: 100.
Amount of theory:5, amount of practice:10, amount of open tasks:5
After filtration: 
Element number 1:
Work checked: 0, student name: Zhenya, mark: 0.
Amount of theory:5, amount of practice:5, amount of open tasks:5
Element number 2:
Work checked: 0, student name: Sheremet, mark: 90.
Amount of theory:5, amount of practice:5, amount of open tasks:5
Element number 3:
Work checked: 0, student name: Kryzha, mark: 100.
Amount of theory:5, amount of practice:5, amount of open tasks:5
```

## Conclusion

In this laboratory work i learned how to work with flows in C++ language.