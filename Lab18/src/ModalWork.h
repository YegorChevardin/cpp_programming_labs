#pragma once
#include "lib.h"
#include "ModalWorkStructure.h"
#ifndef MODALWORKH
#define MODALWORKH

/** Class of modal control work */
class ModalWork {
    private:
	bool checked;
	string student_last_name;
	int mark;
	ModalWorkStructure work_structure;
	WorkSubject work_subject;

    public:
	/** ModalWork default constructor */
	ModalWork();
	/** ModalWork regular constructor */
	explicit ModalWork(string input);

	/** ModalWork copy constructor */
	ModalWork(const ModalWork &copy);

	/** ModalWork destructor */
	~ModalWork();

	bool getChecked() const;
	void setChecked(bool checked);

	string getStudentLastName() const;
	void setStudentLastName(string student_last_name);

	int getMark() const;
	void setMark(int mark);

	ModalWorkStructure &getWorkStructure();
	void setWorkStructure(const ModalWorkStructure &work_structure);

	WorkSubject getWorkSubject() const;
	void setWorkSubject(WorkSubject work_subject);

	/** Converts object to string
     * @return string with objects data
     */
	string ModalWorkToString();
};

#endif
