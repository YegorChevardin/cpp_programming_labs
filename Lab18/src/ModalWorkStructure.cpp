#include "ModalWorkStructure.h"

/* Init of ModalWorkStructure class methods */
ModalWorkStructure::ModalWorkStructure(string input)
{
	stringstream input_stream(input);

	input_stream >> ModalWorkStructure::amount_of_open_exercises;
	input_stream >> ModalWorkStructure::amount_of_theoretical_questions;
	input_stream >> ModalWorkStructure::amount_of_practical_exercises;
}

ModalWorkStructure::ModalWorkStructure(const ModalWorkStructure &copy)
{
	ModalWorkStructure::amount_of_practical_exercises =
		copy.amount_of_practical_exercises;
	ModalWorkStructure::amount_of_theoretical_questions =
		copy.amount_of_theoretical_questions;
	ModalWorkStructure::amount_of_open_exercises =
		copy.amount_of_open_exercises;
}

ModalWorkStructure::~ModalWorkStructure() = default;

int ModalWorkStructure::getAmountOfPracticalExercises() const
{
	return ModalWorkStructure::amount_of_practical_exercises;
}
void ModalWorkStructure::setAmountOfPracticalExercises(int practise)
{
	ModalWorkStructure::amount_of_practical_exercises = practise;
}

int ModalWorkStructure::getAmountOfTheoreticalQuestions() const
{
	return ModalWorkStructure::amount_of_theoretical_questions;
}
void ModalWorkStructure::setAmountOfTheoreticalQuestions(int theory)
{
	ModalWorkStructure::amount_of_theoretical_questions = theory;
}

int ModalWorkStructure::getAmountOfOpenExercises() const
{
	return ModalWorkStructure::amount_of_open_exercises;
}
void ModalWorkStructure::setAmountOfOpenExercises(int open_task)
{
	ModalWorkStructure::amount_of_open_exercises = open_task;
}

string ModalWorkStructure::ModalWorkStructureToString()
{
	stringstream output;

	output << "Amount of theory:"
	       << ModalWorkStructure::amount_of_theoretical_questions
	       << ", amount of practice:"
	       << ModalWorkStructure::amount_of_practical_exercises
	       << ", amount of open tasks:"
	       << ModalWorkStructure::amount_of_open_exercises << endl;

	string result = output.str();

	return result;
}
