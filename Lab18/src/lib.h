#pragma once
#ifndef LIBH
#define LIBH

#include <iostream>
#include <string>
#include <sstream>
#include <cstdio>
#include <fstream>
#include <cstdlib>

using std::cerr;
using std::cout;
using std::endl;
using std::getline;
using std::ifstream;
using std::ofstream;
using std::string;
using std::stringstream;

/** Constant that stands for max mark that student can ever get */
#define MAX_MARK 100

/** Constant for default size of dynamic array (can be changed) */
#define DYNAMIC_ARRAY_SIZE 1

/** Subjects for students */
enum WorkSubject { PROGRAMMING, ALGORITHMS, ELECTRONIC };

#endif //LIBH
