/**
 * # Laboratory work number 18
 * Work with classes
 * @author Chevardin Y.
 * @date 5-May-2022
 * @version 1.0.0
 */

#include "lib.h"
#include "entity.h"
#include "ModalWork.h"

#include "ModalWork.cpp"
#include "ModalWorkStructure.cpp"

/**
 * Place of enter
 * @return code of ending (0)
 */
int main()
{
	const string INPUT = "assets/input.txt";
	const string OUTPUT = "assets/output.txt";

	DynamicArrayContainer worksArrayContainer("0");

	worksArrayContainer.getFromFile(INPUT);

	string result = worksArrayContainer.toString();

	cout << result;

	cout << "After filtration: " << endl;

	auto *Zhenya = new ModalWork();
	Zhenya->setStudentLastName("Zhenya");
	worksArrayContainer.addElement(*Zhenya);
	worksArrayContainer.sortWorksByQuestions();
	worksArrayContainer.writeToFile(OUTPUT);

	cout << worksArrayContainer.toString();

	free(Zhenya);

	return 0;
}
